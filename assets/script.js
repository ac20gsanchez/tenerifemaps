// JSONs con su nombre, descripción y URL
var geojsons = {
    "transporte" : [
            "Transporte", 
            "Relación de recursos georeferenciados relacionados con el transporte (aparcamientos, paradas de taxi) en la Isla de Tenerife",
            "https://www.tenerifedata.com/dataset/0203e3b8-a104-490d-9fa3-1a2b99ae462f/resource/2c02587f-654f-4781-95ee-0bdb5f42bd38/download/transportes.geojson"
    ],
    "alimentacion" : [
            "Alimentación",
            "Comercios de alimentación en la isla de Tenerife",
            "https://www.tenerifedata.com/dataset/2656f8fb-02d1-46d1-983e-547900a8cd8f/resource/e22c346a-07c2-47f8-a361-6749b64c4ddc/download/alimentacion.geojson"
    ],
    "educacion_y_cultura" : [
            "Educacion y cultura",
            "Listado de recursos georeferenciados relacionados con la educación y la cultura en la isla de Tenerife",
            "https://www.tenerifedata.com/dataset/98ed96ec-ce7d-42da-9e77-5415086c3ad2/resource/fb929dbc-8803-41d9-91ad-2b1ce0df78f6/download/educacionycultura.geojson"
    ],
    "comercio" : [
            "Comercio",
            "Mapas e información georeferenciada de comercios y alimentación en Tenerife",
            "https://www.tenerifedata.com/dataset/3bbb763b-5cd7-42e3-bcdb-039e155972e2/resource/1514847b-2caa-4d68-bc1f-0778ab4b6c38/download/comercio.geojson"
    ],
    "industria" : [
            "Indústria",
            "Actividades calificadas de industrial en la isla de Tenerife",
            "https://www.tenerifedata.com/dataset/742505e1-73ba-4525-b3cc-26d804a11623/resource/3b99f7f7-a0ce-46a5-83f3-70d77b7e4699/download/industria.geojson"
    ],
    "asociaciones" : [
            "Asociaciones",
            "Listado de recursos georeferenciados en el ámbito de las asociaciones ciudadanas en la Isla de Tenerife",
            "https://www.tenerifedata.com/dataset/72871919-a4aa-48aa-8a04-1d03e0d8cb5e/resource/f708f513-4c44-4f6c-b2b7-f51b04996d10/download/asociacionesciudadania.geojson"
    ],
    "hosteleria_y_restauracion" : [
            "Hosteleria y restauración",
            "Listado de locales de hostelería y restauración en la isla de Tenerife",
            "https://www.tenerifedata.com/dataset/8f1efe35-483c-41fd-9087-1faf20b2bf4a/resource/7b46dbb0-3688-454f-8db5-4a4a1d0d380c/download/hosteleriayrestauracion.geojson"
    ],
    "servicios_publicos" : [
            "Servicios públicos",
            "Relación de recursos georeferenciados de Administraciones Públicas y Servicios Públicos en la isla de Tenerife",
            "https://www.tenerifedata.com/dataset/f0790c1b-316d-4e52-a803-98e347eca64b/resource/da798ddf-c8bb-4bc2-8fa5-6bada29499e5/download/administracionyserviciospublicos.geojson"
    ],
    "turismo" : [
            "Turismo",
            "Alojamientos hoteleros, extrahoteleros y agencias de viajes",
            "https://www.tenerifedata.com/dataset/ccf4f74a-4cfc-4a99-b264-16c4bfe96f55/resource/936400ac-b6a5-4969-8fef-0387a0d2fed2/download/turismo.geojson"
    ],
    "agricultura" : [
            "Agricultura",
            "Relación de recursos de comercio georeferenciados correspondientes al ámbito de la Agricultura",
            "https://www.tenerifedata.com/dataset/f42a050c-6ebc-420c-ac00-be5e15cb844a/resource/38566b6c-1141-482b-bf1b-c88a7cb6cea6/download/agricultura.geojson"
    ],
    "locales_disponibles" : [
            "Locales disponibles",
            "Relación de locales sin ocupar susceptibles de actividad comercial",
            "https://www.tenerifedata.com/dataset/c5c12ce2-1c31-45ce-8955-be66d6b9cf58/resource/37000f37-57ce-4c80-82ff-143f9feadb5c/download/localesdisponibles.geojson"
    ],
    "medicina_y_salud" : [
            "Medicina y salud",
            "Información georeferenciada y listados de centros de salud, farmacias, clínicas dentales y otros servicios sanitarios",
            "https://www.tenerifedata.com/dataset/d1826fd4-38e1-4e9a-a01e-560e035690e4/resource/b078b36c-82b2-4178-9915-495c9407670d/download/medicinaysalud.geojson"
    ],
    "deporte_y_ocio" : [
            "Deporte y ocio",
            "Relación de recursos georeferenciados relacionados con el deporte y ocio en la isla de Tenerife",
            "https://www.tenerifedata.com/dataset/15da863d-b428-4e4b-a602-2ca447445ec4/resource/2e185e22-18d9-4ad3-998f-bb6ca00bf0e4/download/deporte_y_ocio.geojson"
    ],
    "servicios" : [   
            "Servicios",
            "Relación de recursos georeferenciados relacionados con el sector servicios en la Isla de Tenerife",
            "https://www.tenerifedata.com/dataset/2390bb93-2f25-4fc4-b49e-df975d139b44/resource/28352dbb-e93d-40c8-b9d4-a1155173d981/download/servicios.geojson"
    ]
};

// JSON escogido para mostrar sus ubicaciones en el mapa
var geojson = 'https://www.tenerifedata.com/dataset/0203e3b8-a104-490d-9fa3-1a2b99ae462f/resource/2c02587f-654f-4781-95ee-0bdb5f42bd38/download/transportes.geojson';

// Cantidad de segundos para que se actualicen los datos solos
// Modificar estas dos variables para cambiar el tiempo de actualización automática de los datos
var segundosActualizarAuto = 50; // Tiempo del contador en segundos
var miliSegundosActualizar = 52000; // Tiempo real en que se ejecuta la actualización para que no haya delay (+2 mili segundos de diferencia con el reloj)
var contadorAutoActualizar = segundosActualizarAuto; // Variable que se usa como cronómetro para la cuenta atrás de actualizar

/**
 * Carga los datos del JSON escogido a una tabla y un mapa
 * @param myMap: Obtiene el objecto del mapa para poder modificar su zoom y otras opciones.
 * @param myRenderer: Obtiene el mapa para añadir y editar su información a mostrar.
 * @param markerGroup: Capa donde guardar todos los marcadores que ha de mostrar el mapa.
 * @param search: Término introducido en el input de búsqueda.
 */
function actualizarDatos(myMap, myRenderer, markerGroup, search) {

    // Muestra un gif de carga mientras se cargan todos los datos del json
    $('.preloader-wrapper').css('visibility', 'visible');

    // Accede al geojson
    $.getJSON(geojson, function (data) {

        // Borra la tabla si existe para poder crear una nueva actualizada
        if($(".tableFixHead").length != 0) {
            $(".tableFixHead").remove();
        }

        var rainbowCounter = 0; // Contador del número de marcadores para añadirles colores distintos a cada uno mediante la función rainbow
        
        var items = []; // Guarda la tabla en html
        var coordenadasMapa = []; // Guarda las coordenadas y el nombre de la ubicación

        //
        // Generar la tabla
        //
        // Header de la tabla
        items.push('<table id="dynamicTable" class="table table-hover table-bordered">' +
                        '<thead class="table-dark">' +
                            '<tr>' +
                                '<th onclick="sortTable(0)">Nombre</th>' + 
                                '<th onclick="sortTable(1)">Referéncia</th>' + 
                                '<th onclick="sortTable(2)">Dirección</th>' + 
                                '<th class="responsiveColumn1" onclick="sortTable(3)">Número</th>' + 
                                '<th class="responsiveColumn1" onclick="sortTable(4)">C.Postal</th>' + 
                                '<th onclick="sortTable(5)">Web</th>' + 
                                '<th onclick="sortTable(6)">Email</th>' + 
                                '<th class="responsiveColumn2" onclick="sortTable(7)">Teléfono</th>' + 
                            '</tr>' +
                        '</thead>');

        /**
         * Recorre el json y construye el body de la tabla a partir de los valores del json
         * @param {JSON} key Indice del valor
         * @param {String} val Texto del valor
         */
        $.each(data.features, function (key, val) {

            // Variables del json
            var nombre = checkNull(val.properties.nombre);
            var referencia = checkNull(val.properties.ref);
            var direccion = checkNull(val.properties.dir);
            var numero = checkNull(val.properties.num);
            var codigoPostal = checkNull(val.properties.cp);
            var web = checkNull(val.properties.web);
            var email = checkNull(val.properties.email);
            var telefono = checkNull(val.properties.tf);

            /**
             * Comprueba si el valor es nulo, si está vacío o si es 0, 
             * en caso de que si devuelve el valor vacío para que no aparezca nada en la celda de la tabla
             * @param {String} value Valor para comprobar si está vacío o si es 0
             * @return {String} El valor corregido
             */
            function checkNull(value) {
                let valueChecked = "";
                if (value != null && value != '' && value != 'null' && value != 0) {
                    valueChecked = value;
                }
                return valueChecked;
            }

            /**
             * Añadir los valores del json a cada fila
             */
            function addRow() {

                // Crea la fila y reemplaza los simbolos y espacios del nombre para que cada fila tenga como id el nombre de la ubicación para poder identificar cada fila
                items.push('<tr id="' + nombre.replace(/[^A-Z0-9]+/ig, "_") + '">' +
                            '<td>' + nombre + '</td>' +
                            '<td>' + referencia + '</td>' +
                            '<td>' + direccion + '</td>' +
                            '<td>' + numero + '</td>' +
                            '<td>' + codigoPostal + '</td>' +
                            '<td>' + web + '</td>' +
                            '<td>' + email + '</td>' +
                            '<td>' + telefono + '</td>' +
                        '</tr>');

                // Guarda las coordenadas si existen para que aparezcan luego en el mapa
                if (val.geometry.coordinates != null && val.geometry.coordinates != '' && val.geometry.coordinates != 'null') {
                    coordenadasMapa.push([val.geometry.coordinates[1], val.geometry.coordinates[0], nombre]); // Coordenadas y nombre de la ubicación
                    rainbowCounter++; // Contador del número de marcadores para añadirles colores distintos a cada uno mediante la función rainbow
                }

            }

            // Filtra las filas a incluir si se ha realizado una búsqueda
            if (search === '' 
                || nombre.toUpperCase().includes(search.toUpperCase()) 
                || referencia.toUpperCase().includes(search.toUpperCase())
                || direccion.toUpperCase().includes(search.toUpperCase())
                || numero.toUpperCase().includes(search.toUpperCase())
                || codigoPostal == search
                || web.toUpperCase().includes(search.toUpperCase())
                || email.toUpperCase().includes(search.toUpperCase())
                || telefono == search ) {

                    addRow();

            } 

            
        });

        // Si no hay datos para mostrar muestra un mensaje en la tabla
        if (rainbowCounter === 0) {
            items.push('<tr>' +
                            '<td>No se ha encontrado ningúna ubicación relacionada con la búsqueda: "' + search + '"</td>' +
                        '</tr>');
            $("#searchNotFound").html('No se ha encontrado ningúna ubicación relacionada con la búsqueda: "' + search + '"');
        } else {
            $("#searchNotFound").html('');
        }

        // Termina de añadir la tabla en html a la variable
        items.push('</table>');
        // Crea un div en el body y inserta la tabla el nuevo div
        $('<div/>', {
            "class":"tableFixHead",
            html:items.join('')
        }).appendTo('#tableDiv');

        //
        // Añade los datos al Mapa
        //
        // Recorre el array de coordenadas y crea cada uno de los marcadores
        markerGroup.clearLayers(); // Borra las ubicaciones actuales del mapa para añadir las actualizadas
        var markers = []; // Para abrir el pop up al pulsar la fila de la ubicación hace falta guardar cada marcador en este array
        var dataColor = 0; // Contador del número de marcadores para añadirles colores distintos a cada uno mediante la función rainbow
        /**
         * Recorre los datos de las coordenadas guardadas
         * @param {Array} element datos de las ubicaciones
         */
        coordenadasMapa.forEach((element) => { // Añade las ubicaciones al mapa
            var trimmedTitle = element[2].replace(/[^A-Z0-9]+/ig, "_").replace(/[#]/g,''); // Titulo del marcador sin espacios para poder referenciar con el id de cada fila
            // Crea el marcador con cada dato
            var circuloUbicacion = L.circleMarker( [element[0], element[1]], {renderer: myRenderer, title: element[2],  color: rainbow(rainbowCounter, dataColor)} ).bindPopup(element[2]);
            circuloUbicacion.addTo(markerGroup); // Añade el marcador a la capa de marcadores
            markers.push(circuloUbicacion); // Añade el marcador al array para poder mostrar el pop up al hacer clic sobre su fila en la tabla

            dataColor++; // Los elementos son las coordenadas, y el título de la ubicación.

            if (trimmedTitle) {
                // Cada fila tendrá la función de acercar el mapa a su ubicación y mostrar el pop up al hacer clic
                $('#' + trimmedTitle).children().click(function() {
                    myMap.flyTo([element[0], element[1]], 16, {
                        animate: true,
                        duration: 3 // segundos
                    });

                    markerFunction(trimmedTitle);
                });
                $('#' + trimmedTitle).css("cursor", "pointer"); // Poner el cursor en modo pointer para las filas que tienen ubicación
            }

        });

        /**
         * Función para abrir el pop up de la ubicación pulsada en la tabla
         * @param {String} titleMarker Título de la ubicación
         */
        function markerFunction(titleMarker){
            for (var i in markers){ // Por cada marcador guardado compara el titulo al id de la fila
                var markerID = markers[i].options.title;
                if ( markerID.replace(/[^A-Z0-9]+/ig, "_") == titleMarker ){
                    markers[i].openPopup();
                }
            }
        }
        
        //
        // Genera los datos de la gráfica
        //
        google.charts.setOnLoadCallback(drawChart); // Función para crear el grafico cuando cargue la API

        // Cuando terminan de cargar todos los datos del json el gif de carga desaparece
        $('.preloader-wrapper').css('visibility', 'hidden');
        $("#updatePopup").show().delay(6000).fadeOut(); // Muestra un texto indicando que los datos se han actualizado correctamente
        
        // Reinicia la cuenta atrás para que se actualicen los datos solos
        contadorAutoActualizar = segundosActualizarAuto;
    
    });

}


/**
 * Muestra un gráfico con los datos del json
 */
function drawChart() {

    //Filtro gráfica
    var titleToFilter;
    var typeToFilter;
    if ( $('#chartFilterUbicaciones').is(':checked') ) {
        // Filtro coordenadas
        titleToFilter = "Cantidad de ubicacines según su sector en tenerife"; 
        typeToFilter = "Ubicaciones";
    } else if ( $('#chartFilterWebs').is(':checked') ) {
        // Filtro páginas web
        titleToFilter = "Cantidad de páginas web según su sector en tenerife";; 
        typeToFilter = "Páginas web";
    }

    var dataChart = new google.visualization.DataTable(); // Genera un gráfico nuevo
    dataChart.addColumn('string', 'Sector'); // La variable dataChart guarda los valores que mostrará la gráfica
    dataChart.addColumn('number', typeToFilter);

    // Genera la gráfica en el elemento indicado
    var chart = new google.visualization.BarChart(document.getElementById('myChart'));

    var chartOptions = { // Opciones de la gráfica
        title: titleToFilter,
        width: $(window).width(),
        backgroundColor: '#000000',
        legendTextStyle: {color: 'white'},
        vAxis: {
            title: 'Sector',
            titleTextStyle: {color: 'white'},
            textStyle: {color: '#bababa'}
        },   
        hAxis: {
            title: 'Cantidad',
            titleTextStyle: {color: 'white'},
            textStyle: {color: '#bababa'}
        },      
        colors: ['lightblue']
    }; // Indica que la gráfica tendrá el mismo tamaño que la ventana

    // Añade los datos básicos a la tabla
    chart.draw(dataChart, chartOptions);

    /**
     * Recorre todos los JSONs y cuenta la cantidad de ubicaciones de cada sector
     * @param {JSON}
     */
    $.each(geojsons, function(key) {
    
        /**
         * Por cada json recorre sus datos
         * @param {JSON}
         */
        $.getJSON(geojsons[key][2], function (data) {
            
            var countChart = 0; // Contador de ubicaciones por cada sector
    
            $.each(data.features, function (key, val) {

                //Filtro gráfica
                var dataToFilter;
                if ( $('#chartFilterUbicaciones').is(':checked') ) {
                    dataToFilter = val.geometry.coordinates; // Filtro coordenadas
                }
                else if ( $('#chartFilterWebs').is(':checked') ) {
                    dataToFilter = val.properties.web; // Filtro páginas web
                }
            
                // Comprueba y suma a un contador que cada ubicación contenga coordenadas válidas o una web según el filtro seleccionado
                if (dataToFilter != null && dataToFilter != '' && dataToFilter != 'null') {
                    countChart++;
                }
    
            });
    
            // Añade la cantidad de ubicaciones del sector a la gráfica 
            dataChart.addRow([geojsons[key][0], countChart]);
            chart.draw(dataChart, chartOptions);
            // Continúa el loop para contar las ubicaciones del siguiente sector en el JSON
        });

    });

}


/**
 * Función para ordenar la tabla haciendo clic en cada header 
 * (El navegador se cuelga unos segundos si hay muchas filas)
 * @param {number} n Indice del header a ordenar
 */
function sortTable(n) {
    // Ordena una tabla fila por fila comparandola con la siguiente y cambiandola de posición
    // Source: https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_sort_table
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("dynamicTable");
    switching = true;
    // Dirección de ordenación en ascendente:
    dir = "asc";
    // Loop para ordenar las tablas:
    while (switching) {
      switching = false; // Variable que indica si se ha ordenado la fila 
      rows = table.rows;
      // Loop de toda la tabla excepto headers:
      for (i = 1; i < (rows.length - 1); i++) {
        shouldSwitch = false; // Variable que indica si hay que ordenar o no esta fila
        x = rows[i].getElementsByTagName("TD")[n]; 
        y = rows[i + 1].getElementsByTagName("TD")[n]; // variable de la siguiente fila para compararse con la actual
        // Comprueba si se han de intercambiar las filas
        if (dir == "asc") { // Comprueba en dirección ascendente
          if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            break; // Si han de intercambiarse pasa la variable a true y termina el loop
          }
        } else if (dir == "desc") { // Comprueba en dirección descendiente
          if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            break; // Si han de intercambiarse pasa la variable a true y termina el loop
          }
        }
      }
      if (shouldSwitch) {
        // Se intercambian las filas
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        switchcount ++; 
      } else {
        // Intercambia la dirección si para preparar la siguiente ordenación en sentido contrario al volver hacer click al header
        if (switchcount == 0 && dir == "asc") {
          dir = "desc";
          switching = true;
        }
      }
    }
}


/**
 * Genera colores RGB de forma equitativa y repartida
 * @param {number} numOfSteps Cantidad de colores a generar
 * @param {number} step Indice del color actual que se va a generar. Ej: 30, 6 -> Generar colores para 30 objetos, mostrar el color del objeto 6 de 30
 * @return {Array} Color para cada objeto
 */
 function rainbow(numOfSteps, step) {
    // Esta funciona genera colores equitativamente para que no se solapen entre ellos. Ideal para que destaquen sobre apps de mapas.
    // Adam Cole, 2011-Sept-14
    // De HSV a RBG - adaptado a partir de: 
    // https://gist.github.com/mjackson/5311256
    var r, g, b;
    var h = step / numOfSteps;
    var i = ~~(h * 6);
    var f = h * 6 - i;
    var q = 1 - f;
    switch(i % 6){
        case 0: r = 1; g = f; b = 0; break;
        case 1: r = q; g = 1; b = 0; break;
        case 2: r = 0; g = 1; b = f; break;
        case 3: r = 0; g = q; b = 1; break;
        case 4: r = f; g = 0; b = 1; break;
        case 5: r = 1; g = 0; b = q; break;
    }
    var c = "#" + ("00" + (~ ~(r * 255)).toString(16)).slice(-2) + ("00" + (~ ~(g * 255)).toString(16)).slice(-2) + ("00" + (~ ~(b * 255)).toString(16)).slice(-2);
    return (c);
}


/**
 * Cuando ha cargado todo el html ejecuta el código
 */
$(document).ready(function () {

    //
    // Genera el mapa
    //
    var myMap = L.map("myMap").setView([28.287729, -16.5239177], 10); // Posición (latitud i longitud) inicial del mapa y nivel de Zoom.
    var myRenderer = L.canvas({ padding: 0.5 });
    // Tipo de vista nivel de Zoom máximo.
    L.tileLayer(`https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png`, {maxZoom: 19,}).addTo(myMap);
    // Crea un una capa de marcadores en lugar de una capa por cada marcador para que el mapa no sea lento cuando haya muchas ubicaciones.
    var markerGroup = L.layerGroup().addTo(myMap);

    // Variable de búsqueda
    var search = "";

    // Actualiza los datos manualmente
    $(document).on("click touchend", "#updatejson, #chartFilterUbicaciones, #chartFilterWebs", function () {
        actualizarTodosLosDatos(); // Actualiza todos los datos
    });

    // Si se busca una ubicación se pasa la variable de búsqueda a la función para actualizar los datos
    $('#searchButton').click(function() {
        // Obtiene el valor de la búsqueda
        search = $("#searchField").val();
        actualizarTodosLosDatos(); // Actualiza todos los datos
    });

    // Resetea el filtro de buscar y muestra todas las ubicaciones de nuevo
    $('#resetSearchButton').click(function() {
        // Resetea el valor de la búsqueda
        search = "";
        $("#searchField").val("");
        actualizarTodosLosDatos(); // Actualiza todos los datos
    });

    // Al pulsar enter se realiza la búsqueda introducida
    $("#searchField").keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            $('#searchButton').click();
        }
    });

    // Centra el mapa
    $('#mapCenterButton').click(function() {
        myMap.flyTo([28.287729, -16.5239177], 10, {
            animate: true,
            duration: 3 // segundos
        });
    });

    // Muestra u oculta la gráfica
    $('#graphicButton').click(function() {
        $('#myChartFilter').toggle();
        $('#myChart').toggle();
        google.charts.setOnLoadCallback(drawChart); // Función para crear la grafica cuando cargue la API
        if($("#myChartFilter").is(":hidden")){ // Cambia el texto del botón del navegador para mostrar u ocultar la gráfica
            $("#graphicButton").html("Mostrar gráfica");
        } else {
            $("#graphicButton").html("Ocultar gráfica");
        }
    });

    /**
     * Botón para cambiar de mapa en el menú
     * @param {JSON} key Indice de los valores del JSON
     */
    $.each(geojsons, function(key) {
        $('#' + key).click(function() {
            geojson = geojsons[key][2]; // Al hacer clic al botón cambia el json a cargar y actualiza la tabla
            $("#titulo").text(geojsons[key][0]); // Cambia los titulos
            $("#subtitulo").text(geojsons[key][1]);
            actualizarTodosLosDatos(); // Actualiza todos los datos
        });
    });

    //
    // Genera la grafica
    //
    google.charts.load('current',{packages:['corechart']}); // Carga la API y el paquete corechart

    // Pasa la variable del mapa y la variable de los marcadores a la función para actualizar todos los datos de la tabla, la gráfica y el mapa.
    actualizarTodosLosDatos();
    actualizarDatos(myMap, myRenderer, markerGroup, search); // En el setInterval se indican los mili segundos los cuales se repetirá la función para actualizar los datos automáticamente.

    // Funciones que ejecutan las funciones para actualizar todos los datos
    var actualizarDatosXtiempo;
    /**
     * Actualiza los datos y establece un intervalo para que se actualicen cada x mili segundos
     */
    function actualizarTodosLosDatos() {
        // Reinicia la función de actualizar los datos cada x mili segundos
        clearInterval(actualizarDatosXtiempo); 
        // Ejecuta la función de actualizar cada mili segundos indicados
        actualizarDatosXtiempo = setInterval(function() {
            actualizarDatos(myMap, myRenderer, markerGroup, search)
        }, miliSegundosActualizar);
        // Actualiza los datos
        actualizarDatos(myMap, myRenderer, markerGroup, search);
    }

    /**
     * Genera una cuenta atrás que indica cuanto falta para que se actualicen los datos automáticamente.
     */
    var contadorWeb = "↻ Actualizar ";
    var int = setInterval(function() {
        if (contadorAutoActualizar >= 0) { // A partir de los segundos indicados en la variable segundosActualizarAuto
            $("#updatejson").val(contadorWeb + contadorAutoActualizar);
            contadorAutoActualizar--; // Si es menor que 0 la cuenta atrás se para
        }
    }, 1000);

    // Responsividad del header
    if ($(window).width() <= 774) {  // Cambia los iconos de encima del mapa para ser responsive
        contadorWeb = "↻ ";
        $("#mapCenterButton").val("↹");
        $("#searchButton").val("🔎︎");
    } else {
        contadorWeb = "↻ Actualizar ";
        $("#mapCenterButton").val("Centrar mapa");
        $("#searchButton").val("Buscar");
    }
    $(window).resize(function(){

        drawChart(); // L gráfica se genera cada vez que se redimensiona la ventana para ser responsive 

        if ($(window).width() <= 774) {  // Cambia los iconos de encima del mapa para ser responsive
            contadorWeb = "↻ ";
            $("#mapCenterButton").val("↹");
            $("#searchButton").val("🔎︎");
        } else {
            contadorWeb = "↻ Actualizar ";
            $("#mapCenterButton").val("Centrar mapa");
            $("#searchButton").val("Buscar");
        }
 
    });
    
});