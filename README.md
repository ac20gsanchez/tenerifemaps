# TenerifeMaps

Ubicaciones y puntos de interés de toda la isla de Tenerife.

## Requisitos

Instalar en el navegador la extensión [CORS](https://mybrowseraddon.com/access-control-allow-origin.html)
Nota: Desactivar CORS al terminar de usar TenerifeMaps ya que pueden aparecer errores en otras webs como YouTube.

## Bugs

Al ordenar alguna columna del mapa, si estás visualizando un sector con muchas ubicaciones como hostelería, el navegador crashea.
Para otros sectores el navegador funciona pero se cuelga y hay que esperar 5-10 segundos hasta que ser ordene la tabla.
